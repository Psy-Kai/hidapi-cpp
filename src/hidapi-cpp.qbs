import qbs

StaticLibrary {
    property string linuxHidInterface: "hidraw"
    PropertyOptions {
        name: "linuxHidInterface"
        allowedValues: [
            "hidraw",
            "libusb"
        ]
        description: "which linux usb interface to use"
    }

    files: []

    cpp.cxxLanguageVersion: "c++11"

    Depends {
        name: "cpp"
    }
}
