from conans import ConanFile


class HidapicppConan(ConanFile):
    name = "hidapi-cpp"
    version = "0.1.0"
    license = "BSD 3-Clausel"
    author = "Kai Dohmen psykai1993@googlemail.com"
    url = "https://gitlab.com/Psy-Kai/hidapi-cpp"
    description = "Reimplementation of Signal11/hidapi in C++"
    topics = ("hid")
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False],
               "linux-hid-interface": ["hidraw", "libusb"]}
    default_options = {"shared": False, "linux-hid-interface": "hidraw"}
    build_requires = "Qbs/1.15.0@psy-kai/testing"

    def requirements(self):
        if self.settings.os == "Linux":
            if self.options["linux-hid-interface"] == "libusb":
                self.requires("libusb/1.0.23")

    def source(self):
        if self.version == "latest":
            self.run("git clone -b master %s.git" % (self.url))
        else:
            self.run("git clone -b v%s %s.git" % (self.version, self.url))

    def build(self):
        args = []
        if self.options.shared:
            args.append("products.hidapi-cpp.type:staticlibrary")
        if self.settings.os == "Linux":
            args.append("products.hidapi-cpp.linuxHidInterface:" +
                        self.options["linux-hid-interface"])
        self.run("qbs %s" % " ".join(args), run_environment=True)

    def package(self):
        self.copy("*", dst="lib", src="lib")
        self.copy("*.h", dst="include", src="include")

    def package_info(self):
        self.cpp_info.libs = ["libhidapi-cpp"]
