import qbs

Project {
    references: [
        "src/src.qbs",
        "tests/tests.qbs",
    ]

    Product {
        name: "misc"

        files: [
            "build.py",
            "conanfile.py",
            ".gitlab-ci.yml",
            "README.md",
            "test_package/conanfile.py",
            "test_package/hidapicpptestpackage.cpp",
            "test_package/hidapicpptestpackage.qbs",
        ]
    }
}
