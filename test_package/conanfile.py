import os
from conans import ConanFile, tools


class HidapicppTestConan(ConanFile):
    settings = "os", "compiler", "build_type", "arch"
    generators = "qbs"

    def build(self):
        args = ["--file %s" % self.source_folder]
        self.run("qbs build %s" % " ".join(args), run_environment=True)

    def test(self):
        if not tools.cross_building(self.settings):
            os.chdir(os.sep.join(["default", "install-root", "usr",
                                  "local", "bin"]))
            self.run(".%shidapicpptestpackage" % os.sep)
