import qbs

CppApplication {
    files: [
        "hidapicpptestpackage.cpp"
    ]

    Depends {
        name: "hidapi-cpp"
    }
}
